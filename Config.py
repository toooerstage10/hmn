import os

class Config(object):
    API_ID = int(os.environ.get("APP_ID", "28083787"))
    API_HASH = os.environ.get("API_HASH", "e62bb88fdbfb83a8f81e031246d79ebf")
    BOT_TOKEN = os.environ.get("BOT_TOKEN", "5925748642:AAGUke9E2Mf-NRyTKc-4TNh5yFH6wuSdbVM")
    STRING_SESSION = os.environ.get("STRING_SESSION", "1AZWarzYBu39rKIaNkKUfxIxMW6wC9fVqfW_AR1bOMwG5fBPRCC5N4_W6h-7dB1CRrotcjk9gDsnlq6lt60TkJX3odXRJrt1SBtW4cS_o_TQxadGPBazY78xK37_MItB8iE7k91w5s88sxT8byzBjSuDHGm3zZAgcg8dnyEhXf3XHhSdCkElpi2_J3CktZobjULV9mI3Nguib5ne53nNH1uD9nAJWAJxZvGDBu_PYdGFnsFceNCLQIaVsaiKuEStFKJvkYr2mwmVF3C7vq-97rjV_U5VHb84IqNbPB9FN6xPvBFJjOf372PBaR_aOca7ka-0xDs8SSN-BHP6Jl4IRABBW4ObDatQ=")
    MANAGEMENT_MODE = os.environ.get("MANAGEMENT_MODE", "ENABLE")
    HEROKU_MODE = os.environ.get("HEROKU_MODE", "ENABLE")
    BOT_USERNAME = os.environ.get("BOT_USERNAME", "TheSarcasticcat_bot")
    SUPPORT = os.environ.get("SUPPORT", "TheSupportChat") # Your Support
    CHANNEL = os.environ.get("CHANNEL", "TheUpdatesChannel") # Your Channel
    START_IMG = os.environ.get("START_IMG", "https://telegra.ph/file/35a7b5d9f1f2605c9c0d3.png")
    CMD_IMG = os.environ.get("CMD_IMG", "https://telegra.ph/file/66518ed54301654f0b126.png")
    ASSISTANT_ID = int(os.environ.get("ASSISTANT_ID", "5867273515")) # telegram I'd not Username
    AUTO_LEAVE_TIME = int(os.environ.get("AUTO_LEAVE_ASSISTANT_TIME", "54000")) # in seconds
    AUTO_LEAVE = os.environ.get('AUTO_LEAVING_ASSISTANT', None) # Change it to "True"
